import json
import time
import yaml
import requests
from deepdiff import DeepDiff
import msgSender
import logging

logging.basicConfig(filename='LOG.log', level=logging.INFO, format='%(asctime)s %(message)s')

with open ("TOKEN.yaml", "r") as f:
        token = yaml.safe_load(f)                       #oppening TOKEN.yaml file to get creds

TOKENwz = (token['tokenWZ'])
headers = {'Accept': 'application/json', 'access-token': TOKENwz}
boardID = token['boardID']
url = ('https://workzen.mts.ru/pubapi/cards?boards_id=' + boardID +'&archive=false')

def cardsList():
        req = requests.get(url, headers=headers).text  #making Request
        print (req)
        numOfCards = len((json.loads(req))['cards']) #getting number of cards to limit eterrations of upcomming While
        j=0 #zeroing counter
        listOfcards = {} #creating Dict to fill it
        print (numOfCards)
        while j<numOfCards: #adding every existed card on board to Dict
        #        print (j)
                cardID = (json.loads(req))['cards'][j]['id'] #Getting cardID from erquest
                execID = (json.loads(req))['cards'][j]['executor_id'] #Getting executor ID from request
                nameofCard = (json.loads(req))['cards'][j]['title'] #getting name of card from request
                listOfcards.update({cardID:[execID, nameofCard]}) #Filling dictionary with values
                j+=1
        print(listOfcards)
        return listOfcards #Returning dict with cardID correlated with ExecutoID and Name of card

def getchangedCards():
        with open ('cardtemp.yaml', 'r') as f:    #opening cardList file to compare with fresh one
                cardsFromFile = yaml.safe_load(f)
        cardslist = cardsList() #assign it to Variable
        difference = DeepDiff(cardsFromFile, cardslist, verbose_level=2) #Getting differences with DeepDiff lib
        print ('[INFO]All the Diff that was found', difference)
        for k in list(difference):  #itterating causes of difference
                print ('[CYCLEinDIFF]This is difference i woring with ', k)
                formattedList = cardParser(k, difference) #Calling cardParcer to create the list of changed Cards. Cause of trigger and list with differences in arguments
                msgDecider(k, formattedList, cardslist, cardsFromFile) #Calling msgDecider to decide, how to notify user
                time.sleep(1)
        with open('cardtemp.yaml', 'w') as f:      #Saving all the changes to file, to make it possible to compare next time
                data = yaml.safe_dump(cardslist, allow_unicode=True)
                f.write(data)

def msgDecider(cause, formattedList, cardslist, cardsFromFile):
        for n in formattedList:   #iterating IDs from fresh made list
                if cause == 'values_changed': #If the value were changed, ID of card is going with extra symbols we need to remove
                        n = n[0:-3]
                print('[formattedList]Starting itteration of Cards list ', n)
                if cause == 'dictionary_item_removed':    #If item was REMOVED, we r comparing with file and not the list of cards in work
                        comapeList = cardsFromFile
                else:                                     #Else its compared vice versa
                        comapeList = cardslist
                for key in comapeList:             #Iterating IDs from reference list
                        print('[cardslist]this is the key from Cards i get ', key)
                        if key == n:             #If there is a match, we sending msg depending on cause, that triggered the change
                                if cause == 'values_changed':
                                        t = ('Card "' + cardslist[key][1] + '" appointed to ' + nameExtractor(cardslist[key][0]))
                                if cause == 'dictionary_item_removed':
                                        t = ('Card named "' + cardsFromFile[key][1] + '" deleted')
                                if cause == 'dictionary_item_added':
                                        if cardslist[key][0] != None:
                                                t = ('Card "' + cardslist[key][1] + '" created and appointed to ' +
                                                     nameExtractor(cardslist[key][0]))
                                        else:
                                                t = ('Card created with name "' + cardslist[key][1] + '"')
                                time.sleep(5)
                                msgSender.sendMSGtoTG(t)


def cardParser(case, differ):
        listOfChangedCards = list(differ[case])
        #Getting changed cards with 1 selected cause
        print ('[cardParser]This is list of changed cards for past 20 seconds')
        print (listOfChangedCards)

        for m in range (len(listOfChangedCards)):
                #There is useless symbols from both sides of ID. Have to remove them
                listOfChangedCards[m] = listOfChangedCards[m].replace('root', '')[2:-2]
        return listOfChangedCards

def usernameCollector():  #getting usernames from board
        req = requests.get(url, headers=headers).text  #making Request
        usersList = {} #creating empty dict
        d=0 #zeroing counter
        while d < len(list(json.loads(req)['members'])):   #iterating Members of board
                ID = json.loads(req)['members'][d]['id'] #getting user's ID
                USERNAME = json.loads(req)['members'][d]['first_name'] #getting name of user
                usersList.update({ID:USERNAME}) #Addin key+value to dict
                d+=1 #increment
        with open('USERS.yaml', 'w') as f:      #Saving names to file
                data = yaml.safe_dump(usersList, allow_unicode=True)
                f.write(data)

def nameExtractor(ID):
        #from User's ID its getting FirstName
        with open('USERS.yaml', 'r') as f:
                names = yaml.safe_load(f)
                name = names[ID]
        return name



#Starting infinite loop
looped = 'yes'
while looped == 'yes':
        try:
                usernameCollector()
                time.sleep(1)
                getchangedCards()
                time.sleep(60)
        except (requests.exceptions.ConnectionError, requests.exceptions.ChunkedEncodingError) as error:
                logging.info('Error occured \n', error)
                time.sleep(60)
                continue


