import yaml
import telegram.ext

with open("TOKEN.yaml", "r") as f:
    token = yaml.safe_load(f)
tokenTG = token['tokenTG']
chatID = token['chatID']
bot = telegram.Bot(token=tokenTG)
def sendMSGtoTG(text):
    bot.send_message(chat_id=chatID, text = text)